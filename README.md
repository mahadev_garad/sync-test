# CI/CD Pipeline for Salesforce DX

## Table of contents

* [GitLab Runner](#gitLab-runner)
* [Input Gitlab CI/CD Pipeline Environment Variables](#input-gitlab-cicd-pipeline-environment-variables)
* [Pipeline Stages](#pipeline-stage)

## GitLab Runner

GitLab Runner is the open source project that is used to run your jobs and send the results back to GitLab.
It is used in conjunction with GitLab CI/CD, the open-source continuous integration service included with GitLab that coordinates the jobs.

We have used __docker runner__.

## Input Gitlab CI/CD Pipeline Environment Variables
Before you run your CI pipeline, you need to enter a few configuration variables to allow GitLab to interact with your Salesforce orgs.
From your GitLab project page, click Settings, CI / CD, then expand Variables.
![env-variables](gitlab-variables.PNG)
Here are the variables to set:<br/>
PACKAGE_NAME<br/>
SERVER_KEY_PASSWORD<br/>
SF_CONSUMER_KEY<br/>
SF_CONSUMER_KEY_DEV<br/>
SF_USERNAME<br/>
SF_USERNAME_DEV<br/>



## Pipeline Stage
Below are the pipeline stages:

![pipeline-stages](gitlab-pipeline.PNG)

__Code-Scanning__:<br/> Here we have used PMD for static code analysis<br/>

__Unit-Test__:<br/> SFDX cli are used to execute unit test<br/>

__Deploy__:<br/> SFDX cli are used to deploy the code<br/>
